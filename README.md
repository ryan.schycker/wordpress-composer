# Demo template for WordPress (Composer)

WordPress is a blogging and lightweight CMS written in PHP.

## Features

* PHP 8.1
* MariaDB 10.4
* Automatic TLS certificates
* Composer-based build

## Post-install

1. Run through the WordPress installer as normal.  You will not be asked for database credentials as those are already provided.

2. This example looks for an optional `wp-config-local.php` in the project root that you can use to develop locally. This file is ignored in Git.

Example `wp-config-local.php`:

```php
<?php

define('WP_HOME', "http://localhost");
define('WP_SITEURL',"http://localhost");
define('DB_NAME', "my_wordpress");
define('DB_USER', "user");
define('DB_PASSWORD', "a strong password");
define('DB_HOST', "127.0.0.1");
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

// These will be set automatically on Platform.sh to a different value, but that won't cause issues.
define('AUTH_KEY', 'SECURE_AUTH_KEY');
define('LOGGED_IN_KEY', 'LOGGED_IN_SALT');
define('NONCE_KEY', 'NONCE_SALT');
define('AUTH_SALT', 'SECURE_AUTH_SALT');
```

## Contribution

* Maintainer: @ryan.schycker
